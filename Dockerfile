# FROM blazemeter/taurus
FROM public.ecr.aws/lambda/python:3.8

ENV S3_BUCKET=s3://season-bucket/Taurus
ARG JMETER_VERSION=5.4.1

RUN /var/lang/bin/python3.8 -m pip install --upgrade pip && \
    yum install -y gcc java bc wget unzip dos2unix

RUN yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm && \
    yum install -y xmlstarlet

RUN pip install bzt && \
    pip install --no-cache-dir awscli

RUN wget https://apache.claz.org//jmeter/binaries/apache-jmeter-${JMETER_VERSION}.zip && \
    mkdir -p /var/task/.bzt/jmeter-taurus/${JMETER_VERSION} && \
    unzip apache-jmeter-${JMETER_VERSION}.zip -d /var/task/.bzt/jmeter-taurus/${JMETER_VERSION}

COPY cli.py /var/lang/lib/python3.8/site-packages/bzt/cli.py
COPY . /var/task
RUN mkdir -p /var/task/bzt-artifacts && \
    touch /var/task/bzt-artifacts/results.xml
RUN dos2unix load.sh && \
    chmod 755 /var/task/load.sh

CMD ["app.handler"]